##
# Don't do fancy things like trying to make debuginfo, et al.
#
%define         __spec_install_post /bin/true
%define         _instdir    /opt/mathematica10

%ifarch x86_64
%define         _math_arch  Linux-x86-64
%endif

Name:           mathematica10
Version:        10.2.0
Release:        0.duke.1
Summary:        Mathematica is a computing and development environment

Group:          Applications/Engineering
License:        Commercial
Source0:        %{name}-%{version}.tar.gz
Source1:        %{name}.sh
BuildRoot:      %{_tmppath}/%{name}-%{version}-root
ExclusiveArch:  x86_64

AutoReqProv:    0
BuildRequires:  desktop-file-utils, shared-mime-info
BuildRequires: redhat-rpm-config
Requires:       libX11, libXmu

%description
Whether it's simple calculations or large-scale computations,
programming, or presenting, Mathematica is the tool of choice across
the technical world. Throughout industry, government, and education,
two million people--from students to Nobel Laureates--use Mathematica
to achieve more.


%prep
%setup -q -n Unix


%install
rm -rf $RPM_BUILD_ROOT
##
# Create installation directories
#
mkdir -pm 755                               \
    $RPM_BUILD_ROOT%{_instdir}              \
	$RPM_BUILD_ROOT/%{_sysconfdir}/%{name} \
    $RPM_BUILD_ROOT%{_datadir}/applications \
    $RPM_BUILD_ROOT%{_bindir}
##
# Invoke the installer in automatic mode.
#
pushd Installer
./MathInstaller -auto -silent               \
    -createdir=y                            \
    -targetdir=$RPM_BUILD_ROOT%{_instdir}   \
    -execdir=.
rm -f $RPM_BUILD_ROOT%{_instdir}/InstallerLog
popd

## Create link in the bindir
sed "s|@@INSTDIR@@|%{_instdir}|g" %{SOURCE1} > %{buildroot}%{_bindir}/%{name}
%{__chmod} 755 %{buildroot}%{_bindir}/%{name}
##
# Make the Licensing directory live in a sane place.
#
#mv $RPM_BUILD_ROOT%{_instdir}/Configuration/Licensing $RPM_BUILD_ROOT/%{_sysconfdir}/%{name}
#ln -s %{_sysconfdir}/%{name} $RPM_BUILD_ROOT%{_instdir}/Configuration/Licensing

##
# Install the desktop file
#
cat <<EOF>$RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop
[Desktop Entry]
Encoding=UTF-8
Name=Mathematica 10 
Comment=Mathematica (%{version}) Technical Computing System
TryExec=%{_instdir}/Executables/Mathematica
Exec=%{_instdir}/Executables/Mathematica %F
Icon=%{_instdir}/SystemFiles/FrontEnd/SystemResources/X/App.Mathematica.64.png
Terminal=false
Type=Application
Categories=Application;Development;
MimeType=application/vnd.wolfram.nb;application/vnd.wolfram.cdf;application/vnd.wolfram.player;application/vnd.wolfram.mathematica.package;application/vnd.wolfram.wl;x-scheme-handler/wolfram+cloudobject;x-scheme-handler/wolframmathematica+cloudobject
EOF
desktop-file-install --vendor duke --delete-original    \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications         \
  --add-category X-Linux-At-Duke                        \
  --add-category Application                            \
  --add-category Development                            \
  $RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop
##
# Duke Mathpass
#
#echo "!mathematica-lic.oit.duke.edu" > $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/mathpass
echo "!mathematica-lic.oit.duke.edu" > $RPM_BUILD_ROOT%{_instdir}/Configuration/Licensing/mathpass
##
# "Fix" manpath
#
pushd $RPM_BUILD_ROOT%{_instdir}/SystemFiles/SystemDocumentation
ln -s Unix man1
popd


%clean
rm -rf $RPM_BUILD_ROOT


%post


%postun


%files
%defattr(-,root,root,-)
#%config %dir %{_sysconfdir}/%{name}
#%config(noreplace) %{_sysconfdir}/%{name}/*
%{_datadir}/applications/*
%{_instdir}
%{_bindir}/%{name}

%doc

%changelog
* Sun Aug 16 2015 Jimmy Dorff <jdorff@phy.duke.edu> 10.1.0-duke.1
- Bump to 10.2
* Tue May 19 2015 Jimmy Dorff <jdorff@phy.duke.edu> 10.1.0-duke.1
- Keep the vendor Java
#* Mon May 14 2013 Mark Turner   <mt73@duke.edu>  9.0.1-0.duke.1
#- Packaging 9.0.1
* Mon Apr 15 2013 Mark Turner   <mt73@duke.edu>  9.0.0-0.duke.0
- Packaging 9.0.0
* Thu Sep  1 2011 Drew Stinnett <drews@duke.edu> 8.0.1-0.duke.1
- Removing brbcompress
* Fri Jul  1 2011 Drew Stinnett <drews@duke.edu> 8.0.1-0.duke.0
- Upgrading for 8.0.1
- Removing compat-libstdc++-33
* Wed Apr 27 2011 Drew Stinnett <drews@duke.edu> 8.0.0-0.duke.2
- Updating for el6
* Mon Nov 22 2010 Drew Stinnett <drew.stinnett@duke.edu> 8.0.0-0.duke.0
- Updating for 8.0
* Wed Apr 29 2009 Drew Stinnett <drew.stinnett@duke.edu> 7.0.0.duke.2
- Made some changes so newer versions can be installed alongside
- Deprecating profile.d scripts, it's screwing things up with multiple versions
* Thu Jan 22 2009 Zach Hill <zach.hill@duke.edu> 7.0.0.duke.1 
- Version 7.0

* Thu Aug 16 2007 Chris Geddings <chris.geddings@duke.edu> 5.2-0.duke.1 
- Version 5.2

* Fri May 20 2005 Konstantin Ryabitsev <icon@linux.duke.edu> 5.1.1.0-0.duke.1
- Version 5.1.1.0
- Refactor to conform to FE standards
- Handle both i386 and x86_64 in one tarball

* Thu Oct 07 2004 Konstantin Ryabitsev <icon@linux.duke.edu> 5.0.1-0.duke.4
- Use Jpackage.org's java.

* Wed Oct 06 2004 Konstantin Ryabitsev <icon@linux.duke.edu> 5.0.1-0.duke.3
- Rename into mathematica5 so it installs side-by-side with mathematica4
- Do not install things in /usr/bin, modify PATH instead using profile 
  scripts.
- Streamline for external release.

* Wed Jun 02 2004 Konstantin Ryabitsev <icon@linux.duke.edu> 5.0.1-0.duke.2.fc2
- Small change to make it build on el3.

* Thu Apr 29 2004 Konstantin Ryabitsev <icon@linux.duke.edu> 5.0.1-0.duke.1.fc2
- Mathematica 5.
- Moving MathLM to a different RPM.
- The installer script looks a lot better now. Yay.
- No more Linux vs Linux-dyn goofyness.
- Yes! No longer requiring ncompress to install!
- Require compat-libstc++
- Depend on java2 instead of sunjava2.

* Sat Jun 14 2003 Konstantin Riabitsev <icon@linux.duke.edu> 4.2-0.duke.5.fc2
- Linux vs Linux-dyn sysid's were causing non-workage. Moving Linux back
  into Linux-dyn after the installation completes and marking the executables
  appropriately.

* Wed Jun 11 2003 Konstantin Riabitsev <icon@linux.duke.edu> 4.2-0.duke.4.fc2
- Putting in a dynamically linked mathlm supplied by wolfram
- Using dynamically linked binaries from the distribution

* Wed Jun 04 2003 Konstantin Riabitsev <icon@linux.duke.edu> 4.2-0.duke.3.fc2
- Miscellaneous fixes.

* Fri May 30 2003 Konstantin Riabitsev <icon@linux.duke.edu>
- Massive spec changes for 9.
- Adapted for Linux@DUKE.

* Tue May 14 2002 Seth Vidal <skvidal@phy.duke.edu>
- rebuild for 7.3 - use new release numbering

* Tue Dec 18 2001 Seth Vidal <skvidal@phy.duke.edu>
- rebuilt for 7.2 - made the symlink to /etc/mathematica relative

* Sun May 27 2001 Seth Vidal <skvidal@phy.duke.edu>
- due to my astounding stupidity I made the symlink go to the wrong place
  /me boggles
* Fri May 11  2001 Seth Vidal <skvidal@phy.duke.edu>
- Made /etc/mathematica nonsense make sense - and made mathpass a config
  file that won't be replaced by future updates
* Thu May 3  2001 Seth Vidal <skvidal@phy.duke.edu>
- fixed the fonts - They must be AT LEAST above abiwords fonts or all
  hell breaks loose.
* Tue Mar 27 2001 Seth Vidal <skvidal@phy.duke.edu>
- mathematica license note - for michael stenner :)
* Mon Mar 19 2001 Seth Vidal <skvidal@phy.duke.edu>
- Fix mathematica execute symlinks (doh!)
- Add Mathematica icon and desktop file for gnome
* Fri Mar 16 2001 Seth Vidal <skvidal@phy.duke.edu>
- fix the mathlm - add the script
* Wed Mar 14 2001 Seth Vidal <skvidal@phy.duke.edu>
- Rebuilt some more of it.
- need to flesh out the mathlm /etc/ more though
* Mon Feb 05 2001 Seth Vidal <skvidal@phy.duke.edu>
- Rebuilt some of it
* Tue Oct 17 2000 Seth Vidal <skvidal@phy.duke.edu>
- Initial package build.
